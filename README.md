# Practice List

- [011](contents/011_README.md)
- [012](contents/012_README.md)
- [013](contents/013_README.md)
- [014](contents/014_README.md)
- [015](contents/015_README.md): Constructor (1)
- [016](contents/016_README.md): Constructor (2)
- [018\*](contents/018_README.md)
- [020\*](contents/020_README.md)
- [022\*](contents/022_README.md)
- [023](contents/023_README.md)
- [029](contents/029_README.md): Reflection
- [031](contents/031_README.md)
- [032](contents/032_README.md)
- [036](contents/036_README.md)

# Reference

- [Java-100practices](https://github.com/JustSystems/java-100practices)
- [SpotBugs](https://spotbugs.readthedocs.io/ja/latest/)
- [Java オンライン環境](https://ideone.com/)
