[back](../README.md)

## 012：初期化子(1)

- 次のプログラムをコンパイル・実行するとどうなるか？（実際に実行させずに解答すること）

```java
public class Knock012 {
    private static boolean flag;
    public static void main(String[] arguments) {
        System.out.println(flag);
    }
}
```

## Answer

print out `False` in the console;

## Notes

- Initializers are executed before any class constructors;
- default value of `boolean` type is `false;
