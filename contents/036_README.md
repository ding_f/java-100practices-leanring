[back](../README.md)

## 036：排他

- 以下のプログラムをスレッドセーフにせよ。
  - 排他処理は極力短くなるようにせよ。

```java
private List<Object> members;
public void add(Object member) {
    members.add(member);
}
public void replace(Object oldMember, Object newMember) {
    if (members.contains(oldMember)) {
        members.remove(oldMember);
        members.add(newMember);
    }
}
```

- 排他「はいた」

## Thread Safety

### Purpose

Multiple threads created from share object variables and this can lead to `thread interference` and `memory consistency errors` when the threads are used to read and update the shared data. Thread safety is used for avoiding unintended data interaction in multi-threaded code.

- [doc](https://docs.oracle.com/javase/tutorial/essential/concurrency/sync.html)

### Solution

- [synchronized statements](https://docs.oracle.com/javase/tutorial/essential/concurrency/locksync.html)

keyword is `synchronized`. And this will specify the object that provides the intrinsic lock.
Note that each object has an intrinsic lock associated with it.

```java
private List<Object> members;
public void add(Object member) {
    synchronized(members){
        members.add(member);
    }
}
public void replace(Object oldMember, Object newMember) {
    synchronized(members){
        if (members.contains(oldMember)) {
            members.remove(oldMember);
            members.add(newMember);
        }
    }
}
```
