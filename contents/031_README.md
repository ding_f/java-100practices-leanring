[back](../README.md)

## 031：アノテーション(1)

- 以下のコードのアノテーションは妥当か妥当ではないか。理由も合わせて解答せよ。
  - getStringClass()メソッドは変更不可とする。

```java
@SuppressWarnings("unchecked")
public class Knock031 {
    public static void main(String[] arguments) {
        Class<String> object = getStringClass();
        System.out.println(object);
    }

    public static Class getStringClass() {
        return String.class;
    }
}
```

[source](https://github.com/JustSystems/java-100practices/tree/master/contents/031)

## Answer

1. no error
2. not proper

```java
public class Knock031 {
    public static void main(String[] arguments) {
        @SuppressWarnings("unchecked")
        Class<String> object = getStringClass();
        System.out.println(object);
    }

    public static Class getStringClass() {
        // return java.lang.String
        return String.class;
    }
}
```

![sample](annotatin.png)

### Difference between string.getClass() & String.class

- getClass() return the [runtime class of the Object](https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html#getClass--)
- class return the static class of the Object

### @SuppressWarnings("unchecked")

- [Class Definiation](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/SuppressWarnings.html)
  - The string "unchecked" is used to suppress unchecked warnings.
  - As a matter of style, programmers should always use this annotation on the most deeply nested element where it is effective. If you want to suppress a warning in a particular method, you should annotate that method **rather than** its class.

## Notes:

Java annotations were added to Java from Java 5

[Lesson: Annotations](https://docs.oracle.com/javase/tutorial/java/annotations/index.html)

- [pre-defined annotation](https://docs.oracle.com/javase/tutorial/java/annotations/predefined.html)

  - @Deprecated
  - @Override: be used in [Interfaces and Inheritance](https://docs.oracle.com/javase/tutorial/java/IandI/index.html).
  - @SuppressWarnings
    - e.g: @SuppressWarnings("unchecked") = @SuppressWarnings(value="unchecked")
  - @SafeVarargs
  - @FunctionalInterface

- Meta-Annotation:A nnotations That Apply to Other Annotations

  - @Retention
  - @Documented
  - @Target
  - @Inherited
  - @Repeatable

- [Type Annotation: annotations can be applied to the use of types, start on Java SE 8](https://docs.oracle.com/javase/tutorial/java/annotations/type_annotations.html)

  - Class instance creation expression: `new @Interned MyObject()`;
  - Type cast: `myString = (@NonNull String) str`;
  - Implements clause
  - Thrown exception declaration:

- Repeating Annotations: start on Java SE 8
  - Step 1: Declare a Repeatable Annotation Type;
  - Step 2: Declare the Containing Annotation Type which must have a value element with an array type;
