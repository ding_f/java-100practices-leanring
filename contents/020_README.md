[back](../README.md)

## 020：ジェネリクス(Generic)

- 以下の配列(はいれつ)`MEDALS`について、ジェネリクスを用いた(もちいる)場合のリストと用いない場合のリストにそれぞれ(respectively)要素(ようそ/item)を入れ、それぞれのリストについて、全要素を順番に出力するためのプログラムを実装せよ。

```java
private static final String[] MEDALS = new String[]{
    "GOLD", "SILVER", "BRONZE"
};
```

## Notes

- [ORACLE Document about Generic Type](https://docs.oracle.com/javase/tutorial/java/generics/types.html)
- [ORACLE Document about Generic Methods](https://docs.oracle.com/javase/tutorial/java/generics/methods.html)
