[back](../README.md)

## 015：コンストラクタ Constructor (1)

- `java.lang.String`型のインスタンス(instance)変数(hen suu)を持つクラスで、コンストラクタでその変数に代入(だい にゅう)すべき値(ち)（should be assigned?）を取る場合と、取らない場合がある。
  取らない場合、デフォルトの文字列(も じ れつ)"no name"を設定する。このクラスを実装せよ。

## Notes

- constructor syntax

```
public class ObjectName {
    ObjectName() {
        ...
    }
}
```
