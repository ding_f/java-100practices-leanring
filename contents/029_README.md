[back](../README.md)

## 029：リフレクション

- java.lang.StringBuffer クラスのインスタンスを生成し append(String)メソッドを呼び出すコードをリフレクションを用いて実装せよ。
  - java.lang.StringBuffer クラス以外のメソッド呼び出しはリフレクションを使用しなくてよい。

## Reference:

- [Trail: The reflection API](https://docs.oracle.com/javase/tutorial/reflect/index.html)
