[back](../README.md)

## 011：変数の初期化順

- 以下の **4 つ** について実行される順に並べよ。
  1. インスタンス変数への宣言（せんげん）時の値の代入
  2. `static`イニシャライザでのクラス変数への値の代入
  3. コンストラクタでのインスタンス変数への値の代入
  4. `final`であり型（かた）がプリミティブ(primitive)ないしは文字列となるクラス変数への宣言時の、定数（ていすう）と解釈（かいしゃく）する値の代入

## Answer:

4 -> 2(when compile) -> 1(when create an instance) -> 3(after create an instance, it will auto invoke the object constructor)

## Note

- No 6th -> No 9th of [12.4.2. Detailed Initialization Procedure](https://docs.oracle.com/javase/specs/jls/se8/html/jls-12.html#jls-12.4.2)
- メンバ変数 includes クラス変数(with `static` / without `new` also works / create while compile) & インスタンス変数(without `static` / after `new`);
- `primitive type`: boolean, byte, char, short, int, long, float, double ( `string` does not belongs to it );
- A reference to a field that is a [constant variable](https://docs.oracle.com/javase/specs/jls/se8/html/jls-4.html#jls-4.12.4) (§4.12.4) must be resolved at compile time to the value V denoted by the constant variable's initializer [Link](https://docs.oracle.com/javase/specs/jls/se8/html/jls-13.html#jls-13.1).

## Reference

- [【２】　クラス変数、インスタンス変数](http://www.kusa.ac.jp/~kajiura/java/console/con_matome.htm);
