[back](../README.md)

## 023：オーバーロード(method overloading)

- 同一の名称で、int 型 1 つを引数に取るメソッドと float 型 1 つを引数に取るメソッドを作成し、それぞれのメソッドでは渡された引数を文字列に変換して返すように実装せよ。

## Note:

- 同一「どういち」
- 名称「めいしょう」
- 引数「ひきすう」
- 変換「へんかん」
- それぞれ(respectively)

## Reference

- [Java11 String Doc](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/String.html)
  - public static String valueOf​(int i)
  - public static String valueOf​(float f)
