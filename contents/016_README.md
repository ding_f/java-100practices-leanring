[back](../README.md)

## 016：コンストラクタ(2)

- 他クラスからインスタンスを作成(saku sei)できないクラスを実装せよ。

## Answer

- `package private (no modifier) -> private`

## Notes

- [Controlling Access to Members of a Class](https://docs.oracle.com/javase/tutorial/java/javaOO/accesscontrol.html)
