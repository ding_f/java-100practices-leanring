[back](../README.md)

## 022：可変長(かへんちょう)引数(ひきすう) = Variable Arguments (Varargs)

- Java コマンドの引数の数が 3 個の時は 1 個目と 2 個目、5 個の時は 2 個目と 3 個目と 5 個目、7 個の時は 1 個目と 4 個目と 6 個目と 7 個目を、同一のメソッドに渡し、そのメソッドでは渡された引数を標準出力に 1 行につき 1 つずつ出力するプログラムを実装せよ。
  - 引数の数が 3,5,7 のいずれでもない場合は usage を出力して復帰コード 2 で終了するようにせよ。

## Notes

- 引数(ひきすう): argument (e.g. function, program, programme)
- 渡す（わたす）:
- 1 行 (ぎょう):
- 復帰(ふっき)
- [Non-Reifiable Types](https://docs.oracle.com/javase/tutorial/java/generics/nonReifiableVarargsType.html)
