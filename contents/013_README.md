[back](../README.md)

## 013：初期化子(2)

- 次のプログラムをコンパイル・実行するとどうなるか？（実際に実行させずに解答すること）

```java
public class Knock013 {
    private static Object n;
    private static final Object HOGE = n.toString();
    public static void main(String[] arguments) {
        System.out.println("HOGE is " + HOGE);
    }
}
```

## Answer

`NullPointerException` error.

## Notes

- toString() will get `NullPointerException` error if the variable is `null`
