[back](../README.md)  
[source](https://github.com/JustSystems/java-100practices/tree/master/contents/032)

## 032：アノテーション(2)

- 「3 で割り切れる数」を示すフィールド用アノテーションを作成し、それを用いてコマンドの第 1 引数に指定された数が 3 で割りきれる数かどうかをチェックするプログラムを実装せよ。

Create an annotation for fields to display "divisible by three numbers". The details is that check whether the first argument is divisible by three.

- Annotation for \*fields: `@IsDivisibleByThree`;
  - @Retention(RetentionPolicy.RUNTIME)
- allow to receive several arguments;

## Notes

- [Project Lombok](https://projectlombok.org/)
  - [@NonNull](https://projectlombok.org/features/NonNull)

## Japanese

- 割り切れる「わりきれる」
- 示す「しめす」
