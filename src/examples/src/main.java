public class main {

    public static void main(String[] args) {
        new main().run();
    }

    public void run() {
        System.out.println("Start practice014 demo");
        practice014 test14 = new practice014();
        test14.test();

        System.out.println("Start practice015 demo");
        practice015 tar1 = new practice015("Peter");
        practice015 tar2 = new practice015();
        tar1.print();
        tar2.print();

        System.out.println("Start practice016 demo");
        // practice016 tar3 = new practice016();
        // tar3.print();

        System.out.println("Start practice031 demo");
        practice031 p31 = new practice031();
        p31.test();
    }

}
