public class practice018 {

    private enum ClassMembers {
        A("子", 1), B("丑", 2), C("寅", 3), D("卯", 4), E("辰", 5), F("巳", 6), G("未", 7);

        private final String name;

        private final int id;

        private ClassMembers(final String name, final int id) {
            this.name = name;
            this.id = id;
        }

        public static ClassMembers getInstanceFromId(final int id) {
            for (final ClassMembers member : ClassMembers.values()) {
                if (member.id == id) {
                    return member;
                }
            }
            return null;
        }

        public static ClassMembers getInstanceFromName(final String name) {
            for (final ClassMembers member : ClassMembers.values()) {
                if (member.name == name) {
                    return member;
                }
            }
            return null;
        }
    }

    public static void main(final String[] args) {
        System.out.println(ClassMembers.getInstanceFromId(1));
        System.out.println(ClassMembers.getInstanceFromName("寅"));
    }
}