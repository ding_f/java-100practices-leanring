import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;
import java.lang.NoSuchFieldException;
import java.lang.IllegalAccessException;

@Retention(RetentionPolicy.RUNTIME)
@interface IsDivisibleByThree {
}

public class practice032 {
    @IsDivisibleByThree
    static int num;

    public static void main(String[] arguments) {
        if (arguments.length >= 1) {
            try {
                num = Integer.parseInt(arguments[0]);

                if (isDivisibleNumberByThree(num)) {
                    System.out.println("IsDivisibleByThree");
                } else {
                    System.out.println("IsNotDivisibleByThree");
                }
            } catch (NumberFormatException | NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private static boolean isDivisibleNumberByThree(int number) throws NoSuchFieldException, IllegalAccessException {
        Field field = practice032.class.getDeclaredField("num");

        if (field.getAnnotation(IsDivisibleByThree.class) == null) {
            throw new RuntimeException("@IsDivisibleByThree is not assigned");
        }

        int fieldNum = (int) field.get(number);
        return (fieldNum % 3 == 0);
    }
}