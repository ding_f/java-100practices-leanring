public class practice015 {

    private String runner;

    practice015() {
//        runner = "no name";
        this("no name");
    }

    practice015(String newRunner) {
        this.runner = newRunner;
    }

    public void print() {
        System.out.println("Runner: " + runner);
    }

    public static void main(String[] args) {
        System.out.println("pracitce 15th");
        System.out.println(new practice015().runner);
        System.out.println(new practice015("Peter").runner);
    }
}
