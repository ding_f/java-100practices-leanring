public class practice023 {

    public String convertToString(final int text) {
        return String.valueOf(text);
    }

    public String convertToString(final float text) {
        return String.valueOf(text);
    }

    public String convertToString(final int x, final int y) {
        return Integer.toString(x+y);
    }

    public static void main(String[] args) {
        System.out.println("Method overloading is allowed in Java");
        System.out.println("Method overloading can differ by the type of parameters");
        System.out.println("Method overloading can also differ by the number of input parameters (or both) ");

        System.out.println(new practice023().convertToString(100));
        System.out.println(new practice023().convertToString(100.1f));
        System.out.println(new practice023().convertToString(58,129));
    }
}