public class practice016 {

    private String runner;

    private practice016() {
//        runner = "no name";
        this("no name");
    }

    private practice016(String newRunner) {
        this.runner = newRunner;
    }

    public void print() {
        System.out.println("Runner: " + runner);
    }

    public static void main(String[] args) {
        System.out.println("pracitce 16th");
        System.out.println(new practice016().runner);
        System.out.println(new practice016("Peter").runner);
    }
}
